const username = "postgres";
const password = "1234567890";

module.exports = {
    "type": "postgres",
    "host": "localhost",
    "port": 5432,
    username,
    password,
    "database": "nest_awesome",
    "synchronize": true,
    "dropSchema": false,
    "logging": true,
    "entities": [__dirname + "/src/**/*.entity.ts", __dirname + "/dist/**/*.entity.js"],
    "migrations": ["migrations/**/*.ts"],
    "subscribers": ["subscriber/**/*.ts", "dist/subscriber/**/.js"],
    "cli": {
        "entitiesDir": "src",
        "migrationsDir": "migrations",
        "subscribersDir": "subscriber"
    }
}