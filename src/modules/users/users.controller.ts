import { Controller, Param } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';

import { UsersService } from './users.service';
import { UserEntity } from 'src/entities/user.entity';

@Crud({
    model: {
        type: UserEntity
    },
    params: {
        id: {
            field: 'id',
            type: 'uuid',
            primary: true
        }
    }
})
@Controller('users')
export class UsersController implements CrudController<UserEntity> {

    constructor(public service: UsersService) { }

}
