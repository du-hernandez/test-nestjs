import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../../entities/user.entity';

@Injectable()
export class UsersService extends TypeOrmCrudService<UserEntity>{

    constructor(@InjectRepository(UserEntity) userRepository: Repository<UserEntity>) {
        super(userRepository);
    }

}
