import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';

const options = new DocumentBuilder()
  .setTitle('Users API')
  .setDescription('Users Api Description')
  .setVersion('1.0')
  .addTag('User, Email, Comment, name, nameDisplay')
  .build();


async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/doc', app, document);

  await app.listen(3000);
}
bootstrap();
